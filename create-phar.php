<?php

$error = "Error! Please specify existing output folder as first argument\n";

if (empty($argv[1])) {
    file_put_contents('php://stdout', 1);
    exit;
}

if (!is_dir($argv[1])) {
    file_put_contents('php://stdout', 2);
    exit;
}

$phar = new Phar(rtrim($argv[1], '/') . '/space-cleaner.phar');
$phar->setDefaultStub('space-cleaner.php', '/(.*)php$/');
$phar->buildFromDirectory(__DIR__);
