<?php

namespace Console;

use TaskManager\Interfaces\AbstractNotifier;
use TaskManager\Notifiers\ConsoleNotifier;
use TaskManager\Notifiers\FileNotifier;
use TaskManager\Notifiers\MailNotifier;
use TaskManager\Notifiers\SlackNotifier;

class NotifiersBuilder
{

    /**
     * @var array class names
     */
    private $notifierDrivers = [
        'console' => ConsoleNotifier::class,
        'mail' => MailNotifier::class,
        'slack' => SlackNotifier::class,
        'file' => FileNotifier::class,
    ];

    /**
     * @param array $notifiersConfigs
     * @return AbstractNotifier[]
     */
    public function buildFromConfig(array $notifiersConfigs)
    {
        $notifiers = [];
        foreach ($notifiersConfigs as $notifierConfig) {
            $className = self::getNotifierClass($notifierConfig['driver']);
            $params = isset($notifierConfig['params']) ? $notifierConfig['params'] : [];
            $tags = isset($notifierConfig['tags']) ? $notifierConfig['tags'] : [];
            $notifiers[$notifierConfig['alias']] = new $className($params, $tags);
        }
        return $notifiers;
    }

    /**
     * @param string $driver
     * @return string
     */
    private function getNotifierClass($driver)
    {
        return $this->notifierDrivers[$driver];
    }

}