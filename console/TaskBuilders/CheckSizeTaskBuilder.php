<?php

namespace Console\TaskBuilders;


use Console\TaskBuilderInterface;
use TaskManager\Tasks\CheckSizeTask;

class CheckSizeTaskBuilder implements TaskBuilderInterface
{
    

    /** @inheritdoc */
    public function build(array $taskConfig)
    {
        return new CheckSizeTask($taskConfig);
    }
}