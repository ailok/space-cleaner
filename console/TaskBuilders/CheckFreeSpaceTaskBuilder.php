<?php

namespace Console\TaskBuilders;


use Console\TaskBuilderInterface;
use TaskManager\Tasks\CheckFreeSpaceTask;

class CheckFreeSpaceTaskBuilder implements TaskBuilderInterface
{

    /** @inheritdoc */
    public function build(array $taskConfig)
    {
        // simple case - just all params goes into constructor
        return new CheckFreeSpaceTask($taskConfig);
    }

}