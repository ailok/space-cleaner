<?php

namespace Console\TaskBuilders;


use Components\DateIntervalTranslator;
use Console\TaskBuilderInterface;
use Exception;
use TaskManager\Interfaces\ResultFormatterInterface;
use TaskManager\Tasks\FilesTask\Actions\DeleteAction;
use TaskManager\Tasks\FilesTask\FilesResultFormatter;
use TaskManager\Tasks\FilesTask\FilesTask;
use TaskManager\Tasks\FilesTask\Filters\ExtensionFilter;
use TaskManager\Tasks\FilesTask\Filters\NoneFilter;
use TaskManager\Tasks\FilesTask\Filters\TimeFilter;
use TaskManager\Tasks\FilesTask\Interfaces\ActionInterface;
use TaskManager\Tasks\FilesTask\Interfaces\FilterInterface;

class FilesTaskBuilder implements TaskBuilderInterface
{
    /**
     * @var array
     */
    private $taskConfig;

    /**
     * @var array [action_alias => ActionClassName, ...]
     */
    private $actionsMap = [
        'delete' => DeleteAction::class
    ];

    /**
     * @var array [formatter_alias => FormatterClassName, ...]
     */
    private $formattersMap = [
        'summary_formatter' => FilesResultFormatter::class
    ];

    /** @inheritdoc */
    public function build(array $taskConfig)
    {
        $this->taskConfig = $taskConfig;

        $taskConfig['action'] = $this->getAction();
        $taskConfig['filter'] = $this->getFilter();
        $taskConfig['resultFormatter'] = $this->getResultFormatter();

        return new FilesTask($taskConfig);
    }

    /**
     * @return ActionInterface
     * @throws Exception
     */
    private function getAction()
    {
        if (!isset($this->taskConfig['action'])) {
            throw new Exception('"action" param is required for FilesTask');
        }

        if (!isset($this->actionsMap[$this->taskConfig['action']])) {
            $msgPattern = "Unknown action [%s]. Possible values: %s";
            $msg = sprintf($msgPattern, $this->taskConfig['action'], implode(', ', array_keys($this->actionsMap)));
            throw new Exception($msg);
        }

        return new $this->actionsMap[$this->taskConfig['action']];
    }

    /**
     * @return FilterInterface
     */
    private function getFilter()
    {
        if (!isset($this->taskConfig['filter'])) {
            return new NoneFilter();
        }

        return $this->buildFilter($this->taskConfig['filter']);
    }

    /**
     * @param array $filterConfig
     * @return FilterInterface
     */
    private function buildFilter(array $filterConfig)
    {
        $builderCallback = $this->getFilterBuilder($filterConfig['type']);

        return $builderCallback($filterConfig['conditions']);
    }

    /**
     * @param string $type
     * @return \Closure
     */
    private function getFilterBuilder($type)
    {
        $builders = [
            'time' => function ($filterConditions) {
                $timeFilter = new TimeFilter();
                $olderThanInSeconds = (new DateIntervalTranslator($filterConditions['older_than']))->getInSeconds();
                $timeFilter->olderThan($olderThanInSeconds);
                return $timeFilter;
            },
            'extension' => function ($filterConditions) {
                $extensionFilter = new ExtensionFilter();
                if (isset($filterConditions['accept'])) {
                    $extensionFilter->setExtensionsToAccept($filterConditions['accept']);
                }
                if (isset($filterConditions['reject'])) {
                    $extensionFilter->setExtensionsToReject($filterConditions['reject']);
                }
                return $extensionFilter;
            },
        ];

        return $builders[$type];
    }


    /**
     * @return ResultFormatterInterface
     */
    private function getResultFormatter()
    {
        return new FilesResultFormatter();
    }
}