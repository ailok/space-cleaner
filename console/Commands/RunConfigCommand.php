<?php

namespace Console\Commands;


use Console\ConfigParser;
use Console\NotifiersBuilder;
use Console\TasksBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TaskManager\TasksExecutor;

class RunConfigCommand extends Command
{

    public function configure()
    {
        $this
            ->setName('run-config')
            ->setDescription("Processes specified config")
            ->addArgument('config_path', InputArgument::REQUIRED, "Path to config file");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $configFilePath = $input->getArgument('config_path');
        $config = ConfigParser::getConfig($configFilePath);

        $notifiers = (new NotifiersBuilder)->buildFromConfig($config['notifiers']);
        $tasks = (new TasksBuilder)->buildFromConfig($config['tasks']);

        (new TasksExecutor($tasks, $notifiers))->executeAllTasks();
    }
}