<?php

namespace Console\Commands;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateTestFilesCommand extends Command
{

    public function configure()
    {
        $this
            ->setName('create-test-files')
            ->setDescription("Helper command for creating dummy files to test on")
            ->addArgument('count', InputArgument::REQUIRED, "Count of files to create")
            ->addOption('path', null, InputOption::VALUE_OPTIONAL, "Path where to create files (default storage/tmp/*)")
            ->addOption('name_prefix', null, InputOption::VALUE_OPTIONAL, "Files content (default [dummy_file_])", 'dummy_file_')
            ->addOption('extension', null, InputOption::VALUE_OPTIONAL, "Files extensions (default [log])", 'log')
            ->addOption('content', null, InputOption::VALUE_OPTIONAL, "Files content (default is file name)")
            ->addOption('subtract_modified_time', null, InputOption::VALUE_OPTIONAL, "Set modification time backwards in seconds");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $count = $input->getArgument('count');
        $path = $input->getOption('path');
        if (is_null($path)) {
            $path = ROOT_DIR . '/storage/tmp';
        }
        $namePrefix = $input->getOption('name_prefix');
        $ext = $input->getOption('extension');
        $content = $input->getOption('content');

        for ($i = 0; $i < $count; $i++) {
            $filePath = $this->createDummyFile($i + 1, $path, $namePrefix, $ext, $content);
            if ($subtractSeconds = $input->getOption('subtract_modified_time')) {
                touch($filePath, time() - $subtractSeconds);
            }
        }
    }
    
    /**
     * @param int $index
     * @param string $path
     * @param string $namePrefix
     * @param string $extension
     * @param string $contents
     * @return string
     */
    private function createDummyFile($index, $path, $namePrefix, $extension, $contents = null)
    {
        $filePath = rtrim($path, '/') . '/' . ltrim($namePrefix, '/') . $index . '.' . $extension;
        if (is_null($contents)) {
            $contents = $filePath;
        }
        file_put_contents($filePath, $contents);
        return $filePath;
    }

}