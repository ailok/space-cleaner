<?php

namespace Console;


use InvalidArgumentException;
use Symfony\Component\Yaml\Parser as YamlParser;

class ConfigParser
{

    /**
     * @param string $configFilePath
     * @return array
     */
    public static function getConfig($configFilePath)
    {
        if (!file_exists($configFilePath)) {
            throw new InvalidArgumentException("Config file doesn't exist");
        }

        $extension = pathinfo($configFilePath, PATHINFO_EXTENSION);

        $parserCallback = self::getConfigParserByExtension($extension);

        return $parserCallback(file_get_contents($configFilePath));
    }


    /**
     * @param string $extension
     * @return \Closure
     */
    private static function getConfigParserByExtension($extension)
    {
        $parsers = [
            'yaml' => function ($fileContents) {
                return (new YamlParser())->parse($fileContents);
            },
            'json' => function ($fileContents) {
                return json_decode($fileContents, true);
            }
        ];

        return $parsers[$extension];
    }

}