<?php

namespace Console;


use Console\TaskBuilders\CheckSizeTaskBuilder;
use Console\TaskBuilders\CheckFreeSpaceTaskBuilder;
use Console\TaskBuilders\FilesTaskBuilder;

class TasksBuilder
{

    /**
     * @var array [task_type => BuilderClassName]
     */
    private $taskBuilders = [
        'files_task' => FilesTaskBuilder::class,
        'check_free_space_task' => CheckFreeSpaceTaskBuilder::class,
        'check_size_task' => CheckSizeTaskBuilder::class
    ];


    /**
     * @param array $tasksConfig
     * @return \TaskManager\Interfaces\AbstractTask[]
     * @throws \Exception
     */
    public function buildFromConfig(array $tasksConfig)
    {
        $tasks = [];
        foreach ($tasksConfig as $taskConfig) {
            $taskType = $this->arrayPull($taskConfig, 'type');
            if (!isset($this->taskBuilders[$taskType])) {
                throw new \Exception(sprintf("Unknown task type: %s", $taskType));
            }
            $taskBuilderClass = $this->taskBuilders[$taskType];
            $tasks[] = (new $taskBuilderClass)->build($taskConfig);
        }
        return $tasks;
    }

    /**
     * Helper to get key from array and delete it
     *
     * @param array $array
     * @param string $key
     * @param mixed (optional) $defaultValue
     *
     * @return mixed
     */
    private function arrayPull(array &$array, $key, $defaultValue = null)
    {
        if (!isset($array[$key])) {
            return $defaultValue;
        }

        $value = $array[$key];
        unset($array[$key]);
        return $value;
    }


}