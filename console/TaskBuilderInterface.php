<?php

namespace Console;

use TaskManager\Interfaces\AbstractTask;


/**
 * This is handlers which understands task config and creates task
 */
interface TaskBuilderInterface
{

    /**
     * @param array $taskConfig
     * @return AbstractTask
     */
    public function build(array $taskConfig);

}