<?php

namespace Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Application;
use Console\Commands\CreateTestFilesCommand;
use Console\Commands\RunConfigCommand;

class Kernel
{

    /**
     * @var Command[]
     */
    private $commands = [
        RunConfigCommand::class,
        CreateTestFilesCommand::class,
    ];

    /**
     * @throws \Exception
     */
    public function run()
    {
        $application = new Application();
        foreach ($this->commands as $commandClassName) {
            $application->add(new $commandClassName);
        }

        try {
            $application->run();
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * @param \Exception $e
     */
    private function handleException($e)
    {
        $msgPattern = "[%s] %s:%s \nFile:%s \nStack trace:%s\n\n";
        $msg = sprintf($msgPattern, get_class($e), $e->getMessage(), $e->getFile(), $e->getTraceAsString());

        // output to console
        file_put_contents('php://stdout', $msg);

        // save into file
        $dir = is_writable(ROOT_DIR) ? ROOT_DIR : '/tmp';
        $logFile = realpath($dir . '/space_cleaner_error_log_' . date('Y-m-d') . '.log', $msg);
        file_put_contents($logFile, $msg);
    }

}