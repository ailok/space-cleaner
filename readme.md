# SPACE-CLEANER (or can be positioned as task manager)

The responsibility of this utility is to perform some scheduled tasks on servers with
given config and notify about results. Works only on linux.


## Usage example

Run `/usr/bin/php /path/to/space-cleaner/space-cleaner.php run-config /path/to/config_file.json` to
process the tasks in config file. Please look at config examples to see what tasks can be performed.

Config files can be created in yaml or json format. You can create as much configs as you want and
then for each set up cron-job with needed time interval.


## Config file structure

Config file has two sections: notifiers & tasks. Notifiers are channels where send messages to, tasks are
actual actions performed at given path. Each config file can contain multiple notifiers as well as multiple
tasks. One task can send message to multiple notifiers (like slack, mail, console... more can be added).

### Tasks

Config structure for one task
* string `type` (required): sets what to do with specified path
* string `path` (required): path to folder where task will be performed
* array|string `notify` (optional): this is `alias` notifier
* list `params` (required): task specified params

For now available tasks types are:
* `check_size_task`: just sends info about size of given file or folder. Task has no additional params.
    Message is following:
    ```
    Info: size of /path/to/dir is 12GB
    ```
    Message would be send each time task is executed.

* `check_free_space_task`: check free space at given path.
    Task has one additional param `lowest_limit`. If it is specified and free
    space at given path is less, then message will be:
    ```
    ATTENTION! 3.56GB left at path /var/www (lowest limit is 4GB)
    ```
    Otherwise message will looks like this:
    ```
    Info: 15GB left at /var/www/
    ```
    One of these messages would be send each time task is executed.
* `files_task`: perform operation on files at specified dir.

    Additional params are
    * string `action`: for now only `delete` value is supported, which will delete files
    * array `filter`: condition for selecting files. Possible values are: `extension` & `time`. Extension filter
      accepts one of two `conditions`: `accept` or `reject`. Time filter accepts one `condition` - `older_than`. Which
      can be simple numeric value - this will be interpret as seconds. Or human readable
      value like `1 day`, `2 hour`, `1 day 12 hours 15 minutes`.

     When no files was deleted message is:
     ```
     No files to delete at /var/www/html/v2/uploads
     ```
     Or:
     ```
     20 files was deleted at /var/www/html/v2/uploads. 12.54MB cleaned.
     ```


### Notifiers

#### Config structure for one notifier
* string `alias` (required): unique name of notifier in config context. Later this name can be used at `notify` param key of task.
* string `driver` (required): which driver to use. Available drivers are: console, slack, mail, file
* array|string: `tags` (optional): tags will be added to message
* list `params` (optional): driver specific params

Messages without tags are just single line string. Messages with tags formatted like this:
```
Tags: tag1, tag2
MESSAGE_FROM_TASK
```

Each notifier can get one special param `server_config_path`. This is absolute or relative path to server.conf file
within pounce project. If it is specified then server name will be parsed from this file (from server.id) and added
to message. Message will look like:

```
Server: SERVER_NAME_FROM_CONFIG_FILE
MESSAGE_FROM_TASK
```




