<?php

namespace Components;


use Exception;

class DateIntervalTranslator
{

    /**
     * @var integer
     */
    private $valueInSeconds;

    /**
     * @param string $value
     */
    public function __construct($value)
    {
        if (is_integer($value)) {
            $this->valueInSeconds = $value;
        } else {
            $this->valueInSeconds = $this->parseIntoSeconds($value);
        }
    }

    /**
     * @param string $value
     * @return int
     * @throws Exception
     */
    private function parseIntoSeconds($value)
    {
        $intervalInSeconds = 0;

        if (preg_match("/(\d+)[\s]*(days|day)/i", $value, $daysMatches)) {
            $intervalInSeconds += 60 * 60 * 24 * $daysMatches[1];
        }

        if (preg_match("/(\d+)[\s]*(hours|hour)/i", $value, $hoursMatches)) {
            $intervalInSeconds += 60 * 60 * $hoursMatches[1];
        }

        if (preg_match("/(\d+)[\s]*(minutes|minute)/i", $value, $minutesMatch)) {
            $intervalInSeconds += 60 * $minutesMatch[1];
        }

        if ($intervalInSeconds == 0) {
            throw new Exception(sprintf("Unknown format [%s] of time filter", $value));
        }

        return $intervalInSeconds;
    }


    /**
     * @return integer
     */
    public function getInSeconds()
    {
        return $this->valueInSeconds;
    }
}