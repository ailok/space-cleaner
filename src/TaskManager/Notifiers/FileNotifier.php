<?php

namespace TaskManager\Notifiers;


use TaskManager\Interfaces\AbstractNotifier;

/**
 * Simply puts output into file
 */
class FileNotifier extends AbstractNotifier
{

    use PounceServerNameTrait;

    /**
     * @var string
     */
    protected $file_path;

    /** @inheritdoc */
    public function __construct(array $params = [], $tags = [])
    {
        parent::__construct($params, $tags);

        if (empty($this->file_path)) { // set default path
            $this->file_path = ROOT_DIR . '/storage/file_notifier_output.txt';
        }
    }

    /** @inheritdoc */
    public function _notify($message)
    {
        $message = sprintf("%s - %s\n", date('Y-m-d H:i:s'), $message);
        file_put_contents($this->file_path, $message, FILE_APPEND);
    }
}