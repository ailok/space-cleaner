<?php

namespace TaskManager\Notifiers;


use TaskManager\Interfaces\AbstractNotifier;

class SlackNotifier extends AbstractNotifier
{

    use PounceServerNameTrait;

    /**
     * @var string
     */
    private $apiEndpoint = 'https://slack.com/api/{method}';

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @param array $params
     * @param array|string $tags
     */
    public function __construct(array $params = [], $tags = [])
    {
        parent::__construct($params, $tags);

        if (empty($this->token)) {
            throw new \InvalidArgumentException('Token param is required');
        }

        if (empty($this->channel)) {
            throw new \InvalidArgumentException('Channel name param is required');
        }
    }

    /** @inheritdoc */
    public function _notify($message)
    {
        $this->postMessage($message);
    }

    /**
     * @param string $text
     */
    private function postMessage($text)
    {
        $this->request('chat.postMessage', [
            'channel' => $this->channel,
            'token' => $this->token,
            'text' => $text,
            'username' => 'space-cleaner'
        ]);
    }

    /**
     * Performs the underlying HTTP request.
     * @param string $method The API method to call.
     * @param array $args An associative array of arguments to pass to the API.
     * @param integer $timeout Set maximum time the request is allowed to take, in seconds.
     * @return array The response as an associative array, JSON-decoded.
     */
    private function request($method, $args = array(), $timeout = 100)
    {
        $url = str_replace('{method}', $method, $this->apiEndpoint);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result ? json_decode($result, true) : false;
    }
}




