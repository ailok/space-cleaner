<?php

namespace TaskManager\Notifiers;


use Assert\Assertion;
use TaskManager\Interfaces\AbstractNotifier;

/**
 * Required params:
 *      - type: sendmail|smtp
 *      - subject: email subject
 *      - to: emails list
 *      - from_email: senders email
 *      - from_name: default is 'space-cleaner'
 *
 * Required params if type == smtp:
 *      - username
 *      - password
 *      - host
 *
 * There are no required params if type == sendmail.
 *
 * Additional optional params:
 *      - errors_log_dir: if there will be some error while sending email they would be logged here (default /tmp)
 */
class MailNotifier extends AbstractNotifier
{

    use PounceServerNameTrait;

    const TYPE_SENDMAIL = 'sendmail';
    const TYPE_SMTP = 'smtp';

    /**
     * @var \PHPMailer
     */
    private $mailer;

    /**
     * @var string sendmail|smtp
     */
    protected $type;

    /**
     * @var string
     */
    protected $subject;

    /**
     * Emails
     * @var array
     */
    protected $to = [];

    /**
     * @var string
     */
    protected $from_email;

    /**
     * @var string
     */
    protected $from_name = 'space-cleaner';

    /**
     * @var string
     */
    protected $errors_log_dir;

    /* ----- SMTP params ----- */

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string ssl|tls
     */
    protected $secureType = 'tls';

    /**
     * @var int
     */
    protected $port = 587;


    /* ----- End of SMTP params ----- */

    public function __construct(array $params, $tags)
    {
        parent::__construct($params, $tags);

        if (!is_array($this->to)) {
            $this->to = [$this->to];
        }
    }

    /** @inheritdoc */
    public function _notify($message)
    {
        try {
            $this->initMailer();

            $this->mailer->setFrom($this->from_email, $this->from_name);
            foreach ($this->to as $recipientEmail) {
                $this->mailer->addAddress($recipientEmail);
            }

            $this->mailer->Subject = $this->formatSubject();;
            $this->mailer->isHTML(true);
            $this->mailer->Body = $message;

            if (!$this->mailer->send()) {
                $this->logError(sprintf("Error while sending email - %s", $this->mailer->ErrorInfo));
            }
        } catch (\Exception $e) {
            $errorMessage = sprintf('%s was thrown. Message: ', get_class($e), $e->getMessage());
            $this->logError($errorMessage);
        }
    }

    /**
     * @param string $message
     * @return string
     */
    protected function formatMessage($message)
    {
        if (!empty($this->tags)) {
            return sprintf("Tags: %s<br />%s", $this->tags, $message);
        }

        return $message;
    }

    /**
     * @return string
     */
    private function formatSubject()
    {
        // --- pounce server name parser
        $serverName = $this->getServerName();
        if (!empty($serverName)) {
            return sprintf('Server: %s; %s', $serverName, $this->subject);
        }
        // --- end of pounce server name parser

        return $this->subject;
    }

    /**
     * Tries to log error somewhere.
     * @param string $errorMessage
     */
    private function logError($errorMessage)
    {
        $errorMessage = sprintf("[%s] %s\n", date('H:i:s'), $errorMessage);
        $fileName = "space_cleaner_mail_errors_" . date('Y-m-d') . ".log";

        if (!empty($this->errors_log_dir) && is_dir($this->errors_log_dir) && is_writable($this->errors_log_dir)) {
            $dir = $this->errors_log_dir;
        } else {
            $dir = '/tmp';
        }

        file_put_contents(rtrim($dir, '/') . '/' . $fileName, $errorMessage, FILE_APPEND);
    }

    /**
     * Initialize mailer object by given type
     */
    private function initMailer()
    {
        $this->validateRequiredParams();

        $this->mailer = new \PHPMailer();

        if ($this->type == self::TYPE_SENDMAIL) {
            return $this->initSendmail();
        }

        if ($this->type == self::TYPE_SMTP) {
            return $this->initSMTP();
        }

        $msgPattern = "Unknown mailer type '%s'. Possible values: [%s]";
        $msg = sprintf($msgPattern, $this->type, implode(',', [self::TYPE_SENDMAIL, self::TYPE_SMTP]));
        throw new \InvalidArgumentException($msg);
    }

    /**
     * @throws \Exception
     */
    private function validateRequiredParams()
    {
        Assertion::allEmail($this->to);
        Assertion::email($this->from_email);
        Assertion::notEmpty($this->subject);
        Assertion::notEmpty($this->from_name);
    }


    private function initSendmail()
    {
        $this->mailer->isSendmail();
    }

    private function initSMTP()
    {
        $this->validateSMTPParams();

        $this->mailer->isSMTP();
        $this->mailer->SMTPAuth = true;
        $this->mailer->SMTPSecure = $this->secureType;
        $this->mailer->Port = $this->port;

        $this->mailer->Host = $this->host;
        $this->mailer->Username = $this->username;
        $this->mailer->Password = $this->password;
    }

    /**
     * @throws \Exception
     */
    private function validateSMTPParams()
    {
        Assertion::notEmpty($this->host, "Host param is required for SMTP mail type");
        Assertion::notEmpty($this->username, "Username param is required for SMTP mail type");
        Assertion::notEmpty($this->password, "Password param is required for SMTP mail type");
        Assertion::notEmpty($this->from_email, "'From email' param is required for SMTP mail type");
        Assertion::inArray($this->secureType, ['ssl', 'tls'], "Secure type can be only ssl or tls");
    }

}