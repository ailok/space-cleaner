<?php

namespace TaskManager\Notifiers;

/**
 * Little changed behavior of notifiers for Pounce project.
 *
 * If notifier receives param 'server_config_path' then it will try to
 * get server name from config file and set it to message.
 */
trait PounceServerNameTrait
{

    /**
     * @var string
     */
    protected $server_config_path;

    /**
     * Adding server name if it was parsed from config
     * 
     * @param string $message
     * @return string
     */
    protected function formatMessage($message)
    {
        $message = parent::formatMessage($message);

        if (empty($this->server_config_path)) {
            return $message;
        }

        $serverName = $this->getServerName();
        if (empty($serverName)) {
            return $message;
        }

        return sprintf("Server: %s\n%s", $serverName, $message);
    }

    /**
     * @return string|null
     */
    protected function getServerName()
    {
        if (!file_exists($this->server_config_path)) {
            return;
        }

        $serverJsonData = file_get_contents($this->server_config_path);
        $serverData = json_decode($serverJsonData, true);
        if (!is_array($serverData)) {
            return;
        }

        if (isset($serverData['server']) && isset($serverData['server']['id'])) {
            return $serverData['server']['id'];
        }
    }

}