<?php


namespace TaskManager\Notifiers;


use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use TaskManager\Interfaces\AbstractNotifier;

class ConsoleNotifier extends AbstractNotifier
{
    
    use PounceServerNameTrait;

    /**
     * @var OutputInterface
     */
    private $output;

    /** @inheritdoc */
    public function __construct(array $params = [], $tags = [])
    {
        parent::__construct($params, $tags);
        $this->output = new ConsoleOutput;
    }

    /** @inheritdoc */
    public function _notify($message)
    {
        $this->output->writeln("<info>{$message}</info>");
    }
}