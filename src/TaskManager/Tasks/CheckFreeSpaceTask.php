<?php

namespace TaskManager\Tasks;


use TaskManager\DefaultResultFormatter;
use TaskManager\Interfaces\AbstractTask;

class CheckFreeSpaceTask extends AbstractTask
{

    /**
     * Path to check free space on
     * @var string
     */
    protected $path;

    /**
     * Lowest limit in megabytes
     * @var int
     */
    protected $lowest_limit;

    /** @inheritdoc */
    public function execute()
    {
        $lowestLimitInBytes = $this->lowest_limit * 1024 * 1024;
        $freeSpaceInBytes = disk_free_space($this->path);

        $freeSpaceInfo = getHumanFilesize($freeSpaceInBytes);

        if (!empty($this->lowest_limit) && $freeSpaceInBytes < $lowestLimitInBytes) {
            return new DefaultResultFormatter(sprintf(
                'ATTENTION! %s left at path %s (lowest limit is %s)',
                $freeSpaceInfo,
                $this->path,
                getHumanFilesize($lowestLimitInBytes)
            ));
        }

        return new DefaultResultFormatter(sprintf("Info: %s left at %s", $freeSpaceInfo, $this->path));
    }


}