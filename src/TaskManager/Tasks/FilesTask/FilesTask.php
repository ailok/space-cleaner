<?php

namespace TaskManager\Tasks\FilesTask;


use TaskManager\Interfaces\AbstractTask;
use TaskManager\Tasks\FilesTask\Interfaces\ActionInterface;
use TaskManager\Tasks\FilesTask\Interfaces\FilesResultFormatterInterface;
use TaskManager\Tasks\FilesTask\Interfaces\FilterInterface;

class FilesTask extends AbstractTask
{

    /**
     * @var string
     */
    protected $path;

    /**
     * @var FilterInterface
     */
    protected $filter;

    /**
     * @var ActionInterface
     */
    protected $action;

    /**
     * @var FilesResultFormatterInterface
     */
    protected $resultFormatter;

    /**
     * @param FilterInterface $filter
     * @return $this
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * @param ActionInterface $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @param FilesResultFormatterInterface $resultFormatter
     */
    public function setResultFormatter(FilesResultFormatterInterface $resultFormatter)
    {
        $this->resultFormatter = $resultFormatter;
    }

    /** @inheritdoc */
    public function execute()
    {
        if (is_null($this->resultFormatter)) {
            $this->resultFormatter = new FilesResultFormatter();
        }

        $files = $this->getFiles($this->path);

        $this->resultFormatter->setPath($this->path);

        $this->resultFormatter->setAction($this->action);

        $this->resultFormatter->setFilesToProcess($files);

        foreach ($files as $filePath) {
            if ($this->filter->isMatchingForProcessing($filePath)) {
                $this->resultFormatter->beforeFileProcessed($filePath);
                $this->action->handleFile($filePath);
                $this->resultFormatter->afterFileProcessed($filePath);
            }
        }

        $this->resultFormatter->end();

        return $this->resultFormatter;
    }

    /**
     * @param string $dir
     * @return array
     */
    private function getFiles($dir)
    {
        $files = [];

        $dh = opendir($dir);
        while (false !== ($filename = readdir($dh))) {

            if (in_array($filename, ['.', '..'])) {
                continue;
            }

            $files[] = realpath(rtrim($dir, '/') . '/' . $filename);
        }

        return $files;
    }
}