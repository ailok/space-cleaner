<?php

namespace TaskManager\Tasks\FilesTask\Interfaces;


interface ActionInterface
{

    /**
     * @param string $filePath
     */
    public function handleFile($filePath);

}