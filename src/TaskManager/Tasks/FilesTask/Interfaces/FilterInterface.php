<?php

namespace TaskManager\Tasks\FilesTask\Interfaces;


interface FilterInterface
{
    /**
     * @param string $filePath
     * @return bool
     */
    public function isMatchingForProcessing($filePath);

}