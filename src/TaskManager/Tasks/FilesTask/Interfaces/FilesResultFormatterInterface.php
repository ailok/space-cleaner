<?php

namespace TaskManager\Tasks\FilesTask\Interfaces;

use TaskManager\Interfaces\ResultFormatterInterface;

interface FilesResultFormatterInterface extends ResultFormatterInterface
{

    /**
     * @param array $files
     */
    public function setFilesToProcess($files);

    /**
     * @param string $path path to folder that is going to be processed by action
     */
    public function setPath($path);

    /**
     * @param ActionInterface $action
     */
    public function setAction(ActionInterface $action);

    /**
     * @param $filePath
     */
    public function beforeFileProcessed($filePath);

    /**
     * @param string $filePath
     */
    public function afterFileProcessed($filePath);

    /**
     * Formats the result
     */
    public function end();
}