<?php

namespace TaskManager\Tasks\FilesTask\Actions;


use TaskManager\Tasks\FilesTask\Interfaces\ActionInterface;

class DeleteAction implements ActionInterface
{

    /** @inheritdoc */
    public function handleFile($filePath)
    {
        $this->foolProof($filePath);
        unlink($filePath);
    }

    private function foolProof($filePath)
    {
        $pathParts = explode('/', $filePath);
        if (count($pathParts) < 3) {
            throw new \Exception("Can not delete path with such little entry level");
        }
    }
}