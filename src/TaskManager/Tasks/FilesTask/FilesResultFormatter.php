<?php

namespace TaskManager\Tasks\FilesTask;


use TaskManager\Tasks\FilesTask\Actions\DeleteAction;
use TaskManager\Tasks\FilesTask\Interfaces\ActionInterface;
use TaskManager\Tasks\FilesTask\Interfaces\FilesResultFormatterInterface;

class FilesResultFormatter implements FilesResultFormatterInterface
{

    /**
     * @var string
     */
    private $path;

    /**
     * @var ActionInterface
     */
    private $action;

    /**
     * @var array files paths
     */
    private $filesToProcess = [];

    /**
     * @var array files paths
     */
    private $processedFiles = [];

    /**
     * @var int in bytes
     */
    private $processedFilesSummarySize = 0;

    /**
     * @var string
     */
    private $text = '';

    /** @inheritdoc */
    public function setFilesToProcess($files)
    {
        $this->filesToProcess = $files;
    }

    /** @inheritdoc */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /** @inheritdoc */
    public function setAction(ActionInterface $action)
    {
        $this->action = $action;
    }

    /** @inheritdoc */
    public function beforeFileProcessed($filePath)
    {
        $this->processedFilesSummarySize += filesize($filePath);
    }

    /** @inheritdoc */
    public function afterFileProcessed($filePath)
    {
        $this->processedFiles[] = $filePath;
    }

    /** @inheritdoc */
    public function end()
    {
        if ($this->action instanceof DeleteAction) {
            $this->text = $this->createDeleteActionMessage();
        } else {
            $this->text = sprintf("%d files processed at path %s", count($this->processedFiles), $this->path);
        }
    }

    /**
     * @return string
     */
    private function createDeleteActionMessage()
    {
        $deletedFilesCount = count($this->processedFiles);

        if ($deletedFilesCount == 0) {
            return sprintf("No files to delete at %s", $this->path);
        }

        $cleanedSpaceInfo = getHumanFilesize($this->processedFilesSummarySize);
        $msgPattern = "%d files was deleted at %s. %s cleaned.";
        return sprintf($msgPattern, count($this->processedFiles), $this->path, $cleanedSpaceInfo);
    }

    /** @inheritdoc */
    public function getText()
    {
        return $this->text;
    }
}