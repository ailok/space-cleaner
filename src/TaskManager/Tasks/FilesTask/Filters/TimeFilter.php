<?php

namespace TaskManager\Tasks\FilesTask\Filters;


use TaskManager\Tasks\FilesTask\Interfaces\FilterInterface;

class TimeFilter implements FilterInterface
{

    /**
     * Timestamp
     * @var int in seconds
     */
    private $deleteFilesBeforeTimestamp;

    /**
     * @param int $seconds
     * @return $this
     */
    public function olderThan($seconds)
    {
        $this->deleteFilesBeforeTimestamp = time() - $seconds;

        return $this;
    }


    /** @inheritdoc */
    public function isMatchingForProcessing($filePath)
    {
        if (is_dir($filePath)) {
            return false;
        }

        $modificationTimestamp = filemtime($filePath);
        
        if ($modificationTimestamp < $this->deleteFilesBeforeTimestamp) {
            return true;
        }

        return false;
    }
}