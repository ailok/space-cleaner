<?php

namespace TaskManager\Tasks\FilesTask\Filters;


use TaskManager\Tasks\FilesTask\Interfaces\FilterInterface;

class NoneFilter implements FilterInterface
{

    /** @inheritdoc */
    public function isMatchingForProcessing($filePath)
    {
        return true;
    }
}