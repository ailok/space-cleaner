<?php

namespace TaskManager\Tasks\FilesTask\Filters;


use TaskManager\Tasks\FilesTask\Interfaces\FilterInterface;

class ExtensionFilter implements FilterInterface
{

    /**
     * @var array
     */
    private $extensionsToAccept = [];

    /**
     * @var array
     */
    private $extensionsToReject = [];


    /**
     * @param string|array $extensions
     * @return $this
     */
    public function setExtensionsToAccept($extensions)
    {
        $this->extensionsToAccept = $this->getMappedExtensions($extensions);

        return $this;
    }

    /**
     * @param string|array $extensions
     * @return $this
     */
    public function setExtensionsToReject($extensions)
    {
        $this->extensionsToReject = $this->getMappedExtensions($extensions);;

        return $this;
    }

    /**
     * Casts argument and apply strtolower for each element
     * @param string|array $extensions
     * @return array
     */
    private function getMappedExtensions($extensions)
    {
        if (!is_array($extensions)) {
            $extensions = [$extensions];
        }

        return array_map(function ($ext) {
            return strtolower($ext);
        }, $extensions);
    }


    /** @inheritdoc */
    public function isMatchingForProcessing($filePath)
    {
        if (is_dir($filePath)) {
            return false;
        }

        $ext = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        if (!empty($this->extensionsToAccept) && !in_array($ext, $this->extensionsToAccept)) {
            return false;
        }

        if (!empty($this->extensionsToReject) && in_array($ext, $this->extensionsToReject)) {
            return false;
        }

        return true;
    }


}