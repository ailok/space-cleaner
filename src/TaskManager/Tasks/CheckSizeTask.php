<?php

namespace TaskManager\Tasks;


use TaskManager\DefaultResultFormatter;
use TaskManager\Interfaces\AbstractTask;

class CheckSizeTask extends AbstractTask
{

    /**
     * @var string
     */
    protected $path;

    /** @inheritdoc */
    public function execute()
    {
        $sizeInBytes = $this->getSize();
        $msg = sprintf("Info: size of %s is %s", $this->path, getHumanFilesize($sizeInBytes));
        return new DefaultResultFormatter($msg);
    }

    /**
     * @return int bytes
     */
    private function getSize()
    {
        if (is_dir($this->path)) {
            return $this->getFolderSizeBash();
        }

        return filesize($this->path);
    }

    /**
     * @return int bytes
     * @deprecated Is more php-friendly, but falling with segmentation fault... Looks like only when there are too much files...
     */
    private function getFolderSize()
    {
        $size = 0;
        foreach (glob(rtrim($this->path, '/') . '/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->getFolderSize($each);
        }
        return $size;
    }

    /**
     * @return int bytes
     * @throws \Exception
     */
    private function getFolderSizeBash()
    {
        $io = popen('/usr/bin/du -sb ' . $this->path, 'r');
        $size = fgets($io, 4096);
        pclose($io);
        if (preg_match('/^(\d+)/', $size, $matches)) {
            return $matches[1];
        }
        throw new \Exception("Size was not get. Output from opened process was [{$size}]");
    }
}