<?php


namespace TaskManager\Interfaces;


abstract class AbstractTask
{
    /**
     * Notifiers aliases
     * @var array
     */
    protected $notify;

    /**
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Runs the task. Fills result handler
     *
     * @return ResultFormatterInterface|null
     */
    abstract public function execute();

    /**
     * Notifiers aliases to send messages to
     *
     * @return array notifiers aliases
     */
    public function getNotifiers()
    {
        if (empty($this->notify)) {
            return;
        }

        if (is_array($this->notify)) {
            return $this->notify;
        }

        return [$this->notify];
    }
}