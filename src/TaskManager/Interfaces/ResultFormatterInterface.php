<?php

namespace TaskManager\Interfaces;


interface ResultFormatterInterface
{

    /**
     * @return string
     */
    public function getText();

}