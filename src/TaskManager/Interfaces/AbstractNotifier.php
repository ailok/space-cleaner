<?php


namespace TaskManager\Interfaces;


abstract class AbstractNotifier
{

    /**
     * @var string
     */
    protected $tags;

    /**
     * @param array $params
     * @param array|string $tags
     */
    public function __construct(array $params = [], $tags = [])
    {
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }

        if (!empty($tags)) {
            $this->tags = is_array($tags) ? implode(', ', $tags) : $tags;
        }
    }

    /**
     * @param string $message
     * @return string
     */
    protected function formatMessage($message)
    {
        if (!empty($this->tags)) {
            return sprintf("Tags: %s\n%s", $this->tags, $message);
        }

        return $message;
    }

    /**
     * @param string $message
     */
    public function notify($message)
    {
        $this->_notify($this->formatMessage($message));
    }

    /**
     * Actual sending of message
     * @param $message
     */
    abstract protected function _notify($message);
}