<?php

namespace TaskManager;

use Exception;
use TaskManager\Interfaces\AbstractNotifier;
use TaskManager\Interfaces\AbstractTask;

class TasksExecutor
{

    /**
     * @var AbstractTask[]
     */
    private $tasks;

    /**
     * @var AbstractNotifier[]
     */
    private $notifiers;

    /**
     * @param array $tasks
     * @param array $notifiers (optional)
     */
    public function __construct(array $tasks, array $notifiers)
    {
        $this->tasks = $tasks;
        $this->notifiers = $notifiers;
    }

    /**
     * Processes all tasks, passes results to notifiers
     */
    public function executeAllTasks()
    {
        foreach ($this->tasks as $task) {
            try {
                $this->processTask($task);
            } catch (\Exception $e) {
                // if task was failed send message about this to all notifiers
                $this->notifyAboutExceptionToAll($task, $e);
            }
        }
    }

    /**
     * @param AbstractTask $task
     * @throws Exception
     */
    private function processTask(AbstractTask $task)
    {
        $resultFormatter = $task->execute();

        if (is_null($resultFormatter)) {
            return;
        }

        $notifiers = $task->getNotifiers();
        foreach ($notifiers as $notifierAlias) {
            $this->notifiers[$notifierAlias]->notify($resultFormatter->getText());
        }
    }

    /**
     * @param AbstractTask $task
     * @param Exception $e
     */
    private function notifyAboutExceptionToAll(AbstractTask $task, $e)
    {
        $msgPattern = "Exception of type [%s] in file [%s] at line [%d} with message [%s] was thrown when executing task [%s]";
        $msg = sprintf($msgPattern, get_class($e), $e->getFile(), $e->getLine(), $e->getMessage(), get_class($task));

        foreach ($this->notifiers as $notifier) {
            $notifier->notify($msg);
        }
    }
}