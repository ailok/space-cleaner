<?php

namespace TaskManager;


use TaskManager\Interfaces\ResultFormatterInterface;

class DefaultResultFormatter implements ResultFormatterInterface
{

    /**
     * @var string
     */
    private $text;

    /**
     * @param string $text
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}