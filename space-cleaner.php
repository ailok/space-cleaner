<?php

require_once __DIR__.'/vendor/autoload.php';

define('ROOT_DIR', __DIR__);

(new \Console\Kernel())->run();
