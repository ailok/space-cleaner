<?php


/**
 * Translate human readable represetation of size
 * 
 * @param int $bytes
 * @param int $decimals
 * @return string
 */
function getHumanFilesize($bytes, $decimals = 2)
{
    $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

/**
 * Translates human readable date interval into seconds
 *
 * @param string $humanDateInterval examples: 1 day, 4 days, 3 hours, 10 minutes, 2 days 12 hours
 * @return int
 */
function parseDateInterval($humanDateInterval)
{
    return (new \Components\DateIntervalTranslator($humanDateInterval))->getInSeconds();
}