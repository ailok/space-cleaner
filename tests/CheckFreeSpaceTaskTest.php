<?php


use TaskManager\Interfaces\ResultFormatterInterface;

class CheckFreeSpaceTaskTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function is_should_notify_when_place_is_not_lower_than_specified()
    {
        $path = '/var';
        $freeSpaceInMegabytes = disk_free_space($path) / 1024 / 1024;
        $lowestLimitInMegabytes = $freeSpaceInMegabytes + 10; // lowest is 10Mb greater then actual free space

        $task = new \TaskManager\Tasks\CheckFreeSpaceTask([
            'path' => $path,
            'lowest_limit' => $lowestLimitInMegabytes,
        ]);

        $result = $task->execute();
        $this->assertInstanceOf(ResultFormatterInterface::class, $result);
    }

}