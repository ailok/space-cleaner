<?php


use TaskManager\DefaultResultFormatter;
use TaskManager\TasksExecutor;
use TestDoubles\NotifierSpy;
use TestDoubles\TaskMock;

class TaskExecutorText extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function it_should_execute_all_tasks()
    {
        $task1 = new TaskMock;
        $task2 = new TaskMock;

        $notifier1 = new NotifierSpy();

        $tasksExecutor = new TasksExecutor([$task1, $task2], [$notifier1]);
        $tasksExecutor->executeAllTasks();

        $this->assertTrue($task1->wasExecuted());
        $this->assertTrue($task2->wasExecuted());
    }

    /** @test */
    public function is_should_notify_all_notifiers()
    {
        $task1 = new TaskMock(['notify' => 'notifier1']);
        $task1->setResultFormatter(new DefaultResultFormatter('task 1 result'));

        $task2 = new TaskMock(['notify' => ['notifier1', 'notifier2']]);
        $task2->setResultFormatter(new DefaultResultFormatter('task 2 result'));

        $notifier1 = new NotifierSpy();
        $notifier2 = new NotifierSpy();

        $tasksExecutor = new TasksExecutor([$task1, $task2], ['notifier1' => $notifier1, 'notifier2' => $notifier2]);
        $tasksExecutor->executeAllTasks();

        $this->assertContains("task 1 result", $notifier1->getMessages());
        $this->assertContains("task 2 result", $notifier2->getMessages());

        $this->assertContains("task 2 result", $notifier2->getMessages());
        $this->assertNotContains("task 1 result", $notifier2->getMessages());
    }

    /** @test */
    public function it_should_not_notify_when_execute_not_return_result_formatter()
    {
        $task1 = new TaskMock(['notify' => 'notifier1']);
        $notifier1 = new NotifierSpy();

        $tasksExecutor = new TasksExecutor([$task1], ['notifier1' => $notifier1]);
        $tasksExecutor->executeAllTasks();

        $this->assertEmpty($notifier1->getMessages());
    }


}