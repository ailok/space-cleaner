<?php

namespace TestDoubles;


use TaskManager\Interfaces\AbstractNotifier;

class NotifierSpy implements AbstractNotifier
{

    /**
     * @var array
     */
    private $messages = [];


    /** @inheritdoc */
    public function __construct(array $params = [])
    {

    }

    /** @inheritdoc */
    public function notify($message)
    {
        $this->messages[] = $message;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}