<?php

namespace TestDoubles;


use TaskManager\Interfaces\AbstractTask;
use TaskManager\Interfaces\ResultFormatterInterface;

class TaskMock extends AbstractTask
{
    /**
     * @var ResultFormatterInterface
     */
    private $resultFormatter;

    /**
     * @var bool
     */
    private $wasExecuted = false;

    /**
     * @param ResultFormatterInterface $resultFormatter
     * @return $this
     */
    public function setResultFormatter(ResultFormatterInterface $resultFormatter)
    {
        $this->resultFormatter = $resultFormatter;
        return $this;
    }

    /** @inheritdoc */
    public function execute()
    {
        $this->wasExecuted = true;
        if (!is_null($this->resultFormatter)) {
            return $this->resultFormatter;
        }
    }

    /**
     * @return boolean
     */
    public function wasExecuted()
    {
        return $this->wasExecuted;
    }


}