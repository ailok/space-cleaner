<?php


use Components\DateIntervalTranslator;

class DateIntervalTranslatorTest extends PHPUnit_Framework_TestCase
{

    /** @test */
    public function it_should_translate_1_day_into_seconds()
    {
        $intervalInSeconds = (new DateIntervalTranslator('1 Day'))->getInSeconds();
        $this->assertEquals(60 * 60 * 24, $intervalInSeconds);
    }

    /** @test */
    public function it_should_translate_2_days_into_seconds()
    {
        $intervalInSeconds = (new DateIntervalTranslator('2 days'))->getInSeconds();
        $this->assertEquals(60 * 60 * 24 * 2, $intervalInSeconds);
    }

    /** @test */
    public function it_should_translate_1_hour_into_seconds()
    {
        $intervalInSeconds = (new DateIntervalTranslator('1 hour'))->getInSeconds();
        $this->assertEquals(60 * 60, $intervalInSeconds);
    }

    /** @test */
    public function it_should_translate_12_hours_into_seconds()
    {
        $intervalInSeconds = (new DateIntervalTranslator('12 Hours'))->getInSeconds();
        $this->assertEquals(60 * 60 * 12, $intervalInSeconds);
    }

    /** @test */
    public function it_should_translate_30_minutes_into_seconds()
    {
        $intervalInSeconds = (new DateIntervalTranslator('30 minutes'))->getInSeconds();
        $this->assertEquals(60 * 30, $intervalInSeconds);
    }

    /** @test */
    public function it_should_return_same_result_if_digit_was_passed()
    {
        $intervalInSeconds = (new DateIntervalTranslator(3600))->getInSeconds();
        $this->assertEquals(3600, $intervalInSeconds);
    }

    /** @test */
    public function it_should_combine_2_days_and_3_hours()
    {
        $intervalInSeconds = (new DateIntervalTranslator("2 days and 3 hours"))->getInSeconds();
        $this->assertEquals((60 * 60 * 24 * 2) + (60 * 60 * 3), $intervalInSeconds);
    }

}