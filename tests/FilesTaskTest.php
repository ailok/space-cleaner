<?php

use TaskManager\Tasks\FilesTask\Actions\DeleteAction;
use TaskManager\Tasks\FilesTask\FilesTask;
use TaskManager\Tasks\FilesTask\Filters\ExtensionFilter;
use TaskManager\Tasks\FilesTask\Filters\NoneFilter;
use TaskManager\Tasks\FilesTask\Filters\TimeFilter;

class FilesTaskTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var string
     */
    private $tmpDirPath;

    public function setUp()
    {
        $this->tmpDirPath = __DIR__ . '/../storage/tmp';
        delete_all_files_in_dir($this->tmpDirPath);
    }

    public function tearDown()
    {
        delete_all_files_in_dir($this->tmpDirPath);
    }

    /** @test */
    public function all_files_in_should_be_deleted_after_every_run()
    {
        create_multiple_files($this->tmpDirPath, "log", 10);
        $this->assertEquals(10, get_files_count_in_dir($this->tmpDirPath), "Test files are not created (Maybe check permissions)");

        $filesTask = new FilesTask(['path' => $this->tmpDirPath]);
        $filesTask->setAction(new DeleteAction());
        $filesTask->setFilter(new NoneFilter);
        $filesTask->execute();

        $this->assertEquals(0, get_files_count_in_dir($this->tmpDirPath), "All files in directory should be deleted, but some remained");
    }

    /** @test */
    public function only_files_with_specified_extension_should_be_deleted_1()
    {
        create_multiple_files($this->tmpDirPath, "log", 7);
        create_multiple_files($this->tmpDirPath, "txt", 5);
        $this->assertEquals(12, get_files_count_in_dir($this->tmpDirPath), "Test files are not created (Maybe check permissions)");

        $filesTask = new FilesTask(['path' => $this->tmpDirPath]);
        $filesTask->setAction(new DeleteAction());
        $filesTask->setFilter((new ExtensionFilter)->setExtensionsToAccept('log'));
        $filesTask->execute();

        $this->assertEquals(5, get_files_count_in_dir($this->tmpDirPath, 'txt'));
        $this->assertEquals(0, get_files_count_in_dir($this->tmpDirPath, 'log'));
    }

    /** @test */
    public function only_files_with_specified_extension_should_be_deleted_2()
    {
        create_multiple_files($this->tmpDirPath, "log", 2);
        create_multiple_files($this->tmpDirPath, "txt", 3);
        $this->assertEquals(5, get_files_count_in_dir($this->tmpDirPath), "Test files are not created (Maybe check permissions)");

        $filesTask = new FilesTask(['path' => $this->tmpDirPath]);
        $filesTask->setAction(new DeleteAction());
        $filesTask->setFilter((new ExtensionFilter)->setExtensionsToReject('txt'));
        $filesTask->execute();

        $this->assertEquals(3, get_files_count_in_dir($this->tmpDirPath, 'txt'));
        $this->assertEquals(3, get_files_count_in_dir($this->tmpDirPath));
        $this->assertEquals(0, get_files_count_in_dir($this->tmpDirPath, 'log'));
    }

    /** @test */
    public function only_files_older_than_10_minutes_should_be_deleted()
    {
        create_multiple_files($this->tmpDirPath, 'log', 3, time() - 5 * 60); // modified 5 minutes ago
        create_multiple_files($this->tmpDirPath, 'log', 5, time() - 20 * 60); // modified 20 minutes ago - should be deleted
        $this->assertEquals(8, get_files_count_in_dir($this->tmpDirPath), "Test files are not created (Maybe check permissions)");

        $filesTask = new FilesTask(['path' => $this->tmpDirPath]);
        $filesTask->setAction(new DeleteAction());
        $tenMinutes = 60 * 10;
        $filesTask->setFilter((new TimeFilter)->olderThan($tenMinutes));
        $filesTask->execute();

        $this->assertEquals(3, get_files_count_in_dir($this->tmpDirPath));
    }

    /** @test */
    public function only_files_older_than_1_day_should_be_deleted()
    {
        create_multiple_files($this->tmpDirPath, 'log', 12); // just modified
        create_multiple_files($this->tmpDirPath, 'txt', 2); // just modified
        create_multiple_files($this->tmpDirPath, 'log', 13, time() - 20 * 60); // modified 20 minutes ago
        create_multiple_files($this->tmpDirPath, 'log', 14, time() - 60 * 60 * 25); // modified 25 hours ago - should be deleted
        $this->assertEquals(41, get_files_count_in_dir($this->tmpDirPath), "Test files are not created (Maybe check permissions)");

        $filesTask = new FilesTask(['path' => $this->tmpDirPath]);
        $filesTask->setAction(new DeleteAction());
        $oneDay = 60 * 60 * 24;
        $filesTask->setFilter((new TimeFilter)->olderThan($oneDay));
        $filesTask->execute();

        $this->assertEquals(27, get_files_count_in_dir($this->tmpDirPath));
        $this->assertEquals(25, get_files_count_in_dir($this->tmpDirPath, 'log'));
        $this->assertEquals(2, get_files_count_in_dir($this->tmpDirPath, 'txt'));
    }

}