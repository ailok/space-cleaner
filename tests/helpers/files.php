<?php


/**
 * @param string $dir
 * @param string $ext
 * @param string $content
 * @param int $modificationTime in seconds
 */
function create_file($dir, $ext, $content = "default content", $modificationTime = null)
{
    $filePath = tempnam($dir, '');

    rename($filePath, $filePath = $filePath . "." . $ext);

    file_put_contents($filePath, $content);

    if (!is_null($modificationTime)) {
        touch($filePath, $modificationTime);
    }
}


function create_multiple_files($dir, $ext, $count, $modificationTime = null)
{
    for ($i = 0; $i < $count; $i++) {
        create_file($dir, $ext, sprintf("%d of %d", $i + 1, $count), $modificationTime);
    }
}


function get_files_count_in_dir($dir, $ext = null)
{
    $count = 0;

    $dh = opendir($dir);
    while (false !== ($filename = readdir($dh))) {

        if (in_array($filename, ['.', '..'])) {
            continue;
        }

        if (!is_null($ext) && strtolower(pathinfo($filename, PATHINFO_EXTENSION)) != strtolower($ext)) {
            continue;
        }

        $count++;
    }

    return $count;
}


function delete_all_files_in_dir($dir, $ext = null)
{
    $dh = opendir($dir);
    while (false !== ($filename = readdir($dh))) {

        if (in_array($filename, ['.', '..'])) {
            continue;
        }

        $filePath = realpath(rtrim($dir, '/') . '/' . $filename);

        if (!is_file($filePath)) {
            continue;
        }

        if (!is_null($ext) && strtolower(pathinfo($filename, PATHINFO_EXTENSION)) != strtolower($ext)) {
            continue;
        }

        unlink($filePath);
    }
}